class SendBlockEvent(object):
    """Represents an instance of a block(s) being propagated from any node to
    any other single node. May be through the network (latency) or through a
    mining pool's intranet (no latency).
    
    Attributes:
        srcId: The ID of the Miner sending the block.
        dest: The Miner object/node receiving the block.
        blocks: The list of Block objects being sent. For all honest mining,
            this list should be always 1 element long.
        time: The float time offset at the moment the block *arrives* at its
            destination.
    """
    def __init__(self, srcId, dest, blocks):
        self.srcId = srcId
        self.dest = dest
        self.blocks = blocks
        self.time = self.blocks[0].time_notified
    
    def generateLogEntry(self):
        if len(self.blocks) == 1:
            return "{0}: Block {1} sent from {2} to {3}.".format(self.time, self.blocks[0].id, self.srcId, self.dest.id)
        else:
            return "{0}: Blocks {1} sent from {2} to {3}.".format(self.time, ", ".join([b.id for b in self.blocks]), self.srcId, self.dest.id)
    
    def handle(self):
        """Handle the event by receiving the block at the destination node. 
        Return any subsequent events (the recipient further forwards the block).
        """
        return self.dest.receiveBlocks(
            self.srcId, self.time, self.blocks, self.generateLogEntry()
        )
    
    def __cmp__(self, other):
        return cmp(self.time, other.time)
