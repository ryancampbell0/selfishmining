from __future__ import division
from Topology import Topology
import logging
import random


class TopologySparse(Topology):
    """A randomized topology where all nodes have a user-defined approximate
    degree. Edges are approximately uniformly randomly distributed. The topology
    is guaranteed to be connected (no disconnected components).
    """
    def __init__(self, nodes, degree=3):
        """Creates a randomized topology as described above with the given
        sparsity. `degree` must be an int (not a float) between 0 and len(nodes)
        inclusive.
        """
        logging.info('Creating a randomized topology with approximate degree %s.' % degree)
        self.graph = {}
        self.groups = {}
        
        for node in nodes:
            node.topology = self
            
            # create edges to nodes outside of group and self
            neighbors = random.sample([n for n in nodes if n.id != node.id and n.groupId != node.groupId], degree)
            self._addClient(node.id, neighbors)

            # directed edges
            for neighbor in neighbors:
                self._addClient(neighbor.id, [node])
        
        self._connectSubcomponents(nodes)

    def _connectSubcomponents(self, nodes):
        """Ensure that there are no disconnected components in the graph by
        doing a depth-first search. Add an edge randomly into a disconnected
        component whenever the search terminates with remaining unexplored
        nodes."""
        exploredGroups = set()
        unexploredGroups = set([n.groupId for n in nodes])
        frontierNodes = [nodes[0]]  # stack (Python lists efficient for this)
        
        while unexploredGroups:
            while frontierNodes:
                node = frontierNodes.pop()
                if node.groupId in exploredGroups:
                    continue
                exploredGroups.add(node.groupId)
                unexploredGroups.discard(node.groupId)
                frontierNodes.extend(self.graph[node.id])
            # DFS done; anything disconnected left? Throw an edge into it and
            # keep going
            if unexploredGroups:
                src = random.sample([n for n in nodes if n.groupId in exploredGroups],1)[0]
                dest = random.sample([n for n in nodes if n.groupId in unexploredGroups],1)[0]
                self.graph[src.id].add(dest)
                self.graph[dest.id].add(src)
                unexploredGroups.discard(dest)
                frontierNodes.append(dest)
        
        bidirectionalEdges = 0
        for node in nodes:
            logging.info(
                "Node %s has degree %s, connected to: %s"
                % (node.id, len(self.graph[node.id]), ", ".join([str(b.id) for b in self.graph[node.id]]))
            )
            bidirectionalEdges += len(self.graph[node.id])
        logging.info("Average degree is %s", bidirectionalEdges/len(nodes))
