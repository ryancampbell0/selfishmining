from Block import Block


class MineBlockEvent(object):
    """Represents an instance of a block being mined. Block may be of any kind
    (honest, selfish, forked & pruned, etc.)
    
    Attributes:
        miner: The Miner object who mined this block.
        time (float): The time offset at the moment the block was mined.
    """
    def __init__(self, miner, time):
        self.miner = miner
        self.time = time
    
    def handle(self):
        """Handle this event by directing its miner to add a new block to the
        head (perhaps selfish head) of their blockchain. Returns the subsequent
        events (miner forwards this block and begins to mine a new block)."""
        events = self.miner.mineBlock(self.time)
        events.append(self.miner.getFutureMineEvent(self.time))
        return events
    
    def __cmp__(self, other):
        return cmp(self.time, other.time)
