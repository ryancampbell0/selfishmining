import random
from SendBlockEvent import SendBlockEvent
import logging


class Topology(object):
    """Class for storing the network topology. Used to create events when
    messages are passed in the network.
    The topology should be static once the simulation starts. No nodes or pools
    within a given Topology instance should ever be changed or deleted.
    This is an abstract class; to instantiate a specific kind of topology use
    one of its subclasses.
    
    Attributes:
        graph: A dictionary mapping a miner ID (int) to a set of Miners that
            are its neighbors. Messages can only be sent by a Miner to its
            neighbors, undergoing network latency/delay.
        groups: A dictionary mapping a group ID (int) to a set of Miners that
            make up the group/pool. Nodes communicating within their own pool
            have zero latency.
    """
    def __init__(self, nodes):
        raise NotImplementedError, "Topology is an abstract class"

    
    def _addClient(self, clientId, neighbors):
        """Add the node with the given clientId to the network, with neighbors a
        list of Miner objects. Only to be called from within this class's
        initializer."""
        if clientId not in self.graph:
            self.graph[clientId] = set(neighbors)
        else:
            self.graph[clientId].update(neighbors)
    
    def addGroup(self, groupId, members):
        """Add the group with the given groupId to the network, with members a
        list of Miner objects. Only to be called when setting up the simulation
        before it has started."""
        assert groupId not in self.groups
        self.groups[groupId] = set(members)
    
    def max_network_latency(self):
        return 0.1
    
    def _random_network_latency(self):
        """Return a random number (in seconds) representing how long it takes
        for a message to arrive at a neighbor node."""
        # uniform distribution for simplicity
        return random.uniform(0.05, self.max_network_latency())  # 50 to 100 ms
    
    def sendBlocks(self, currentTime, clientId, blocks, groupId=None):
        """Returns a list of SendBlockEvents representing the node with ID
        `clientId` sending out a list of Blocks `blocks` at the given global
        time offset `currentTime` (float).
        
        If groupId is None (not passed in), then the block is being sent to its
        network neighbors (equivalent to "publish block"). It will be scheduled
        to arrive at each neighbor with some random latency.
        
        If groupId is an int, the block is being communicated to the pool with
        that ID (invariant: the given clientId must be a member of that pool).
        It will be scheduled to arrive at each pool member with zero latency.
        """
        sendBlockEvents = []
        
        if groupId is None:
            recipients = self.graph[clientId]
        else:
            recipients = self.groups[groupId]
        
        for recipient in recipients:
            if groupId is None:
                arrivalTime = currentTime + self._random_network_latency()
            else:
                arrivalTime = currentTime
            
            if not all([recipient.blockchain.contains(b.id) for b in blocks]):
                sendBlockEvents.append(SendBlockEvent(
                    clientId, recipient, [b.copy(arrivalTime) for b in blocks]
                ))
        
        return sendBlockEvents
