from __future__ import division
from Simulator import Simulator
import random


HONEST_POOL_SIZES = [1 for _ in range(11)]
HONEST_POOL_POWERS = [.01, .01, .01, .02, .03, .04, .05, .07, .08, .15, .25]  # wait, let user set alpha
N_SELFISH_CLIENTS = 40  # in real life, this number would have to be ~560 times greater for the same effect
N_SELFISH_MINERS = 1

# may be the strings 'off', 'random', or 'salted'
PATCH = 'off'

PLOTTING = 'off'

VERBOSE = False  # set to True to display race info; should be False if PLOTTING is 'on'


if __name__ == '__main__':
    if PLOTTING == 'on':
        for i in range(1):
            #ALPHA = i*1.0/210
            ALPHA = 0.28
            if (ALPHA == 0):
                continue
            while True:
                try:
                    nHonestPools = len(HONEST_POOL_SIZES)
                    HONEST_POOL_POWERS = [(1 - ALPHA)/nHonestPools for _ in range(nHonestPools)]
                    Simulator.SELFISH_GROUP_IDS = []
                    s = Simulator(
                        HONEST_POOL_SIZES, HONEST_POOL_POWERS,
                        N_SELFISH_CLIENTS, N_SELFISH_MINERS, ALPHA
                    )

                    s.run(10000)
                    selfishNodeConsult = None
                    for node in s.nodes:
                        if node.isSelfish:
                            selfishNodeConsult = node
                    while selfishNodeConsult.privateBranchLen > 0:
                        s.run(1)
                    s.printStatistics()
                    break
                except KeyError:
                    continue

    else:
        ALPHA = raw_input("Enter alpha: ")
        while True:
            try:
                ALPHA = float(ALPHA)
                break
            except ValueError:
                ALPHA = raw_input("Invalid input. Try again: ")
        
        nHonestPools = len(HONEST_POOL_SIZES)
        #HONEST_POOL_POWERS = [(1 - ALPHA)/nHonestPools for _ in range(nHonestPools)]
        
        s = Simulator(
            HONEST_POOL_SIZES, HONEST_POOL_POWERS,
            N_SELFISH_CLIENTS, N_SELFISH_MINERS, ALPHA
        )
        
        while True:
            print "\nEnter the number of blocks to mine (0 to stop): ",
            while True:
                try:
                    string = raw_input()
                    x = int(string)
                    #x = 1000
                    break
                except ValueError:
                    print "Invalid input. Try again: ",
            if x == 0:
                break
            else:
                print "\nMining %i blocks..." % x
                s.run(x)
                s.printStatistics()
