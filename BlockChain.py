import random
import hashlib
from Block import Block

GENESIS_BLOCK = Block(None, 0.0, -1, None)

def _hash(input_string):
    """Because I got sick of typing this out"""
    return hashlib.sha256(input_string).hexdigest()


class BlockChain(object):
    """Represents an instance of the block chain as stored on one node.
    Nodes do not have to agree on what the state of the block chain is; i.e.
    there is no global block chain state, only a state per-node.
    
    Attributes:
        _blockAdjacencies: A dictionary of (blockId, [next blocks]) pairs
            (thus the block chain is internally represented as a directed graph
            where past blocks point to future blocks). Organized in this way to
            make it fast to traverse the block chain forward (as a Block's
            previousId attribute already makes it easy to traverse the block
            chain backward). For each block, its [next blocks] will likely be
            only a one-element list, but may be more than one when forks are
            involved.
        _blocks: A dictionary of (blockId, Block object) pairs, to obtain a
            Block object on the blockchain given its Id.
        _mainChain: A list of Blocks; a memoized copy of this blockchain's main
            chain.
        _mainChainBlocks: A memoized copied subdictionary of _blocks, used to
            quickly look up whether a given block is in the main chain or not.
        owner: The Miner whose copy of the BlockChain this is.
    """
    def __init__(self, owner):
        """Makes a new blockchain starting with the given genesisBlock object,
        owned by the given Miner `owner`.
        """
        genesisBlock = GENESIS_BLOCK.copy(0.0)
        self._blockAdjacencies = {genesisBlock.id: []}
        self._blocks = {genesisBlock.id: genesisBlock}
        self._mainChain = [genesisBlock]
        self._mainChainBlocks = {genesisBlock.id: genesisBlock}
        self.owner = owner

    def contains(self, blockID):
        """Quick wrapper to check if a block with the given blockID is present
        in this blockchain."""
        return blockID in self._blocks
    
    def head(self):
        """Quick wrapper to get the Block at the head of this blockchain."""
        return self._mainChain[-1]
    
    def addBlocks(self, new_blocks):
        """Adds the given blocks to the blockchain. For each block, uses its
        previousId to determine where to put it; it should most likely be the
        head of the blockchain but may be farther back if a fork is being
        performed. Throws a KeyError if no block with id previousId exists in
        this blockchain (attempt to mine/add illegal block)."""
        for i in range(len(new_blocks)):
            b = new_blocks[i]
            self._blocks[b.id] = b
            self._blockAdjacencies[b.id] = []
            self._blockAdjacencies[b.previousId].append(b)
            # don't print racing messages if there are still more blocks to come
            self._updateMainChain(b, i == len(new_blocks) - 1)
    
    def _updateMainChain(self, new_block, verbose):
        """Update the memoized mainchain with the new block provided. Returns
        void.
        
        If the block is on the (head of the) mainchain, simply append it.
        If the block is off the mainchain, determine if it affects the
        mainchain. This only occurs if the new_block's branch is of equal length
        or greater compared to the previous mainchain branch. In this case,
        update the main chain to the correct branch.
        
        If `verbose` is True, print debugging messages for if a race occurs."""
        from __main__ import PATCH, VERBOSE  # avoid circular import
        
        if not VERBOSE:
            # force debugging messages off
            verbose = False
        
        if new_block.previousId == self.head().id:
            self._mainChain.append(new_block)
            self._mainChainBlocks[new_block.id] = new_block
            return
        
        newBranch = [new_block]
        # Find the branch that leads to the first common ancestor with the
        # previous main chain
        while newBranch[0].id not in self._mainChainBlocks:
            newBranch.insert(0, self._blocks[newBranch[0].previousId])
        
        oldBranch = [self.head()]
        # Find the mainchain branch off of the common ancestor
        while oldBranch[0].id != newBranch[0].id:
            oldBranch.insert(0, self._blocks[oldBranch[0].previousId])
        
        # now that the branches have been identified, delete the common ancestor
        # from the front so that each branch only contains its distinct blocks
        oldBranch.pop(0)
        newBranch.pop(0)
        
        if len(newBranch) < len(oldBranch):
            # new block's fork is too short; no effect on mainchain
            return
        elif len(newBranch) == len(oldBranch):
            from Simulator import SELFISH_GROUP_IDS  # debugging; circular import
            
            # no need to check more than these two branches; anything else would
            # already have been checked here earlier
            possibleChains = [oldBranch, newBranch]
            
            if oldBranch[-1].groupId == self.owner.groupId:
                pickedBranch = oldBranch
            elif newBranch[-1].groupId == self.owner.groupId:
                pickedBranch = newBranch
            else:
                
                
                if PATCH == 'random':
                    pickedBranch = random.choice(possibleChains)
                elif PATCH == 'salted':
                    listOfHashes = [_hash(chn[-1].id) for chn in possibleChains]
                    listOfHashes.sort()
                    salt = ''.join(listOfHashes)
                    pickedBranch = min(
                        possibleChains,
                        key=lambda chain: _hash(chain[-1].id + salt)
                    )
                elif PATCH == 'off':
                    # take advantage of how Python lexicographically sorts lists
                    # of floats; it's important that all lists here are the same
                    # length
                    pickedBranch = min(
                        possibleChains,
                        key=lambda chain: [blck.time_notified for blck in chain]
                    )
                    if verbose:
                        for chain in possibleChains:
                            if chain[0].groupId not in SELFISH_GROUP_IDS:
                                print "honest chain"
                            else:
                                print "selfish chain"
                            print [block.time_notified for block in chain]
                            print ''
                else:
                    assert False, "Error: constant PATCH is " + `PATCH`
            
            # debug output
            if verbose:
                if self.owner.groupId not in SELFISH_GROUP_IDS:
                    if (oldBranch[-1].groupId not in SELFISH_GROUP_IDS
                            and newBranch[-1].groupId in SELFISH_GROUP_IDS):
                        if pickedBranch == newBranch:
                            print "Honest miner mining on the selfish branch!!"
                        elif pickedBranch == oldBranch:
                            print "Honest miner mining on an honest branch"
                    elif (newBranch[-1].groupId not in SELFISH_GROUP_IDS
                            and oldBranch[-1].groupId in SELFISH_GROUP_IDS):
                        if pickedBranch == oldBranch:
                            print "Honest miner mining on the selfish branch!!"
                        elif pickedBranch == newBranch:
                            print "Honest miner mining on an honest branch"
                    else:
                        print "Honest miners forking themselves"
                #else:
                #    if (oldBranch[-1].groupId not in SELFISH_GROUP_IDS
                #            and newBranch[-1].groupId in SELFISH_GROUP_IDS):
                #        if pickedBranch == newBranch:
                #            print "Selfish miner mining on the selfish branch"
                #        else:
                #            assert False, "Selfish miner mining on an honest branch???"
        else:
            pickedBranch = newBranch
        
        if pickedBranch == newBranch:
            # on the main chain, throw out oldBranch and replace with newBranch
            for old_block in reversed(oldBranch):
                del self._mainChainBlocks[old_block.id]
                self._mainChain.pop()
            for new_block in newBranch:
                self._mainChainBlocks[new_block.id] = new_block
                self._mainChain.append(new_block)
            
    
    def getMainChain(self):
        """Returns an ordered list of blocks from past to future representing
        the main chain, **according to the node with this copy of the
        blockchain**. When network latency or secret blocks are involved, nodes
        will disagree on what the main chain is, especially for races."""
        return self._mainChain
