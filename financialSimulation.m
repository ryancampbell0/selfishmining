function f = financialSimulation()

    function miningRev = selfish(p,gamma)
        miningRev = (p.*(1-p).^2.*(4*p+gamma.*(1-2*p))-p.^3)./(1-p.*(1+(2-p).*p));
    end

startPower = 0.28;

totalHashrate = 4932000000000; %Gh/hour
otherHashrate = (1-startPower)*totalHashrate;
hashGrowthFactor = exp((log(1.5) - log(0.38))/365); %growth factor per day
totalRevenue = 72917; %per hour
conversionRateGrowth = exp((log(466.62) - log(233.75))/365); %rate at which conversion rate grows

energyCost = 0.08;
partsMarkup = 0.2; % 20% markup on AntMiner S7
machineCost = 533/(1+partsMarkup); %cost per AntMiner S7 paid by BITMAIN
hashRateEach = 17496000; % Gh/hour
energyUsage = 1293; % watts
energyCostPerHour = energyUsage/1000 * energyCost;

endPower = 0.28:0.01:0.49;

days = 365;
otherMiningRates = repmat(otherHashrate*hashGrowthFactor.^(0:days-1), length(endPower), 1);

hashrateAdded = diag(1./(1 - endPower))*(diag(endPower)*(otherMiningRates + startPower*totalHashrate) - startPower*totalHashrate);
totalMachinesAdded = hashrateAdded / hashRateEach;

machinesPurchasedEachDay = totalMachinesAdded;
for d=days:-1:2
    machinesPurchasedEachDay(:,d) = machinesPurchasedEachDay(:,d) - machinesPurchasedEachDay(:,d-1);
end
equipmentCostPerDay = machinesPurchasedEachDay*machineCost;
extraHonestRevenuePerDay = repmat(24*totalRevenue*(endPower - startPower)',1,days) .*...
    repmat(conversionRateGrowth.^(0:days-1),length(endPower),1);
electricityCostsPerDay = totalMachinesAdded*energyCostPerHour*24;
extraHonestProfitPerDay = extraHonestRevenuePerDay - electricityCostsPerDay - equipmentCostPerDay;
cumulativeHonestProfit = cumsum(extraHonestProfitPerDay,2);

figure1 = figure;
set(figure1, 'position', [123   316   835   549])
a = subplot(1,2,1);
contourf(repmat(0:days-1,length(endPower),1), cumulativeHonestProfit, repmat(endPower',1,days),15)
xlabel('Time (days)')
ylabel('Cumulative profit increase ($)')
axis([1 days min(min(cumulativeHonestProfit)) 3*10^7])
title({'Honest Mining'});
% Create textbox
annotation(figure1,'textbox',...
    [0.0548847420417124 0.932258635478645 0.917672886937431 0.0512668999414543],...
    'String',{'Comparison of AntPool Cumulative Profit Over Time'},...
    'HorizontalAlignment','center',...
    'FontWeight','bold',...
    'FontSize',14,...
    'FitBoxToText','off',...
    'LineStyle','none');
set(a,'position',[0.08 0.08 0.38 0.8])
for gamma=0:0.5:1
    extraSelfishRevenuePerDay = repmat(totalRevenue*24*(selfish(endPower, gamma)' - startPower),1,days) .*...
        repmat(conversionRateGrowth.^(0:days-1),length(endPower),1);
    extraSelfishProfitPerDay = extraSelfishRevenuePerDay - electricityCostsPerDay - equipmentCostPerDay;
    cumulativeSelfishProfit = cumsum(extraSelfishProfitPerDay,2);

    b = subplot(1,2,2);
    contour(repmat(0:days-1,length(endPower),1), cumulativeSelfishProfit, repmat(endPower',1,days),750)
    axis([1 days min(min(cumulativeHonestProfit)) 3*10^7])
    xlabel('Time (days)')
    title(b, ['Selfish Mining, Gamma = ', num2str(gamma)]);
    colorbar
    annotation(figure1,'textbox',...
    [0.0548847420417124 0.932258635478645 0.917672886937431 0.0512668999414543],...
        'String',{'Comparison of AntPool Cumulative Profit Over Time'},...
        'HorizontalAlignment','center',...
        'FontWeight','bold',...
        'FontSize',14,...
        'FitBoxToText','off',...
        'LineStyle','none');
    set(b,'position',[0.51, 0.08, 0.38, 0.8])
    drawnow
%     index = (round(gamma/0.04) + 1);
%     M(index) = getframe(figure1);
end
% movie2avi(M, 'video.avi')

end
