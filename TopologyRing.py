from Topology import Topology
import logging


class TopologyRing(Topology):
    """A symmetric topology where nodes are arranged in a ring such that there
    are `n` selfish nodes in between every honest node (thus all nodes have
    exactly 2 neighbors, and all honest nodes have both their neighbors as
    selfish if n>0. The number of selfish nodes must equal n*the number of
    honest nodes, and there must be at least 3 nodes in the topology so that all
    nodes have 2 distinct neighbors. The effect is that for a given individual\
    (poolless) honest node, a selfish node's message will likely or almost
    certainly (depending on n) arrive before any other honest node's message can
    arrive.
    """
    def __init__(self, honestNodes, selfishNodes, n):
        assert n*len(honestNodes) == len(selfishNodes)
        # no rings of size 2 or less; no doubled edges
        assert len(honestNodes) + len(selfishNodes) >= 3
        logging.info('Creating a ring topology with n=%i and %i honest nodes.' % (n, len(honestNodes)))
        self.graph = {}
        self.groups = {}
        
        # will pop from node lists; copy and store lengths ahead of time
        honestNodes = honestNodes[:]
        selfishNodes = selfishNodes[:]
        numHonestNodes = len(honestNodes)
        
        interleavedNodes = []
        for i in range(numHonestNodes):
            interleavedNodes.append(honestNodes.pop(0))
            for _ in range(n):
                interleavedNodes.append(selfishNodes.pop(0))
        
        # now that we have the list of nodes as arranged around the ring...
        
        for i in range(len(interleavedNodes)):
            # kludge: use Python's negative indices to avoid wraparound ifs
            # with nodes 0 1 2 3, node -1 has neighbors -2 and 0
            # while node 2 has neighbors 1 and 3
            # so loop through indices -1..2 instead of 0..3
            node = interleavedNodes[i-1]
            node.topology = self
            
            self._addClient(
                node.id, [interleavedNodes[i-2], interleavedNodes[i]]
            )
