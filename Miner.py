from __future__ import division  # make / always do float division
import random
import math
from Block import Block
from BlockChain import BlockChain
from MineBlockEvent import MineBlockEvent
import logging


class Miner(object):
    """Represents a node in the Bitcoin network. Nodes may be part of a mining
    pool or they may be individual, and they may or may not be honest. A node
    may have zero hashpower; if so, then it is a non-mining node that only
    serves to propagate blocks.
    
    Attributes:
        id (int): The id of this client.
        topology: The Topology object that this client is a part of. Effectively
            gives this client access to its network neighbors and fellow pool
            members.
        blockchain: The version of the BlockChain that this node sees.
        power (0 <= float <= 1): The relative hashpower of this miner alone.
        groupId (int): The mining pool this node belongs to (if individual
            miner, pool has size 1).
    
    Forbidden Attributes:
        isSelfish (bool): Must only be accessed by Simulator or for debugging/
            logging purposes, never anywhere else.
    """
    _idCount = -1  # static attribute
    
    def __init__(self, power, groupId, isSelfish=False):
        """Create a client with the given relative hashpower and groupId. If
        power is 0, this is a non-mining node. Automatically assigns the miner
        an ID and its own copy of the blockchain containing only the genesis
        block."""
        Miner._idCount += 1
        self.id = Miner._idCount
        self.blockchain = BlockChain(self)
        self.power = power
        self.groupId = groupId
        self.isSelfish = isSelfish
        # set up later as Topology requires all nodes to be constructed first
        self.topology = None
        
        output = 'Created '
        output += 'selfish ' if self.isSelfish else 'honest '
        output += 'miner with ' if self.power > 0 else 'client with '
        output += 'hashpower %f and groupID %i' % (self.power, self.groupId)
        print output
        logging.info(output)
    
    def hasThisBlock(self, blockId):
        """Wrapper to check if this miner has already received news of a Block
        with the given blockId. For an honest miner, the only thing to check is
        their copy of the blockchain. Overridable by a selfish miner to check
        their private chain as well."""
        return self.blockchain.contains(blockId)
    
    def receiveBlocks(self, _, currentTime, blocks, logEntry):
        """Receives a notification of the creation of a new block(s) (either by
        itself or by a message passed by another node). Adds the received
        block(s) to the blockchain in the correct spot. Ensures that the
        block(s) are not already present; if they are, do not add it again and
        do not send it to neighbors.
        
        If the blocks are added to the blockchain, log the provided log entry.
        
        Return the subsequent SendBlockEvents if any (i.e. the messages that
        will propogate this block to its neighbors)."""
        events = []
        
        if not all([self.blockchain.contains(b.id) for b in blocks]):
            logging.info(logEntry)
            self.blockchain.addBlocks(blocks)
            # send to pool members and also neighbors
            events.extend(self.topology.sendBlocks(currentTime, self.id, blocks, self.groupId))
            events.extend(self.topology.sendBlocks(currentTime, self.id, blocks))
        
        return events
    
    def mineBlock(self, time):
        """Mines a new block, then (for code simplicity) notifies itself of that
        block's arrival."""
        prev_head_block = self.blockchain.head()
        new_block = Block(prev_head_block.id, time, self.id, self.groupId)
        logEntry = self.logMinedBlock(time, new_block)
        return self.receiveBlocks(self.id, time, [new_block], logEntry)
    
    def logMinedBlock(self, time, new_block):
        return ("{0}: Block {1} mined by miner {2}.".format(time, new_block.id, self.id))
    
    def _exponential_random(self, lamb):
        """Copied from HW1 P4 lol
        
        Quantized by multiplying the output by 600 so that lamb can be used to
        represent hashpower; e.g. a miner with hashpower 1 expects to find a
        block every 600 seconds; a miner with hashpower 0.1 expects to find a
        block every 6,000 seconds; etc."""
        assert lamb > 0
        return 600 * math.log(1 - random.random())/-lamb
    
    def getFutureMineEvent(self, currentTime):
        """Sample from an exponential distribution scaled by the mining power to
        generate the duration of time that must pass until a block is mined. 
        Add this mining duration to the input 'currentTime' to generate a MineBlockEvent
        with time equal to 'currentTime + mineTime'.
        
        Note: Choose the correct block to use as the previous block. This decides which branch
        the block belongs to. 
        
        Create and return a new-block event."""
        time_to_mine = self._exponential_random(self.power)
        new_time = currentTime + time_to_mine
        return MineBlockEvent(self, new_time)
