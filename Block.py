import random

class Block(object):
    """Represents one node's view of a mined block. Organized using instances of
    BlockChain objects.
    
    Instance attributes:
        id (int): The id of this block.
        previousId (int): The id of this block's parent; None if genesis block.
        time_mined (float): Time offset of when this block was mined.
        time_notified (float): Time offset of when this block arrived at the
            node currently storing it. Necessary to break ties in forks (using
            the original protocol without patch).
        groupId (int): The ID of the mining pool who mined this block; None for
            genesis block.
    
    Forbidden attributes:
        creatorId (int): The ID of the lone miner (within a known pool) who
            mined this block; -1 for genesis block.
    
    Forbidden attributes are unknown to all entities in a simulation and are
    never to be accessed during one, only afterwards for statistical analysis.
    """
    _idCount = -1  # static attribute
    
    def __init__(self, previousId, time_mined, creatorId, groupId):
        self.id = self._getUniqueId()
        self.previousId = previousId
        self.time_mined = time_mined
        self.time_notified = time_mined
        self.creatorId = creatorId
        self.groupId = groupId
    
    def _getUniqueId(self):
        """Increment a static ID for simplicity. Much easier to debug than
        random IDs."""
        #return str(random.SystemRandom().randint(0, 2**64-1))
        Block._idCount += 1
        return str(Block._idCount)
    
    def copy(self, timeNotified):
        """Returns a copy of this block, except with the time_notified attribute
        changed. Required when propogating the existence of this block to the
        network, as other nodes will disagree on when they saw it.
        """
        copied_block = Block(
            self.previousId, self.time_mined, self.creatorId, self.groupId
        )
        Block._idCount -= 1
        copied_block.id = self.id
        copied_block.time_notified = timeNotified
        return copied_block
