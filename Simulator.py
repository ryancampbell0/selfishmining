from __future__ import division
from Miner import Miner
from SelfishMiner import SelfishMiner
import logging
import time
import heapq
from MineBlockEvent import MineBlockEvent
from TopologyRing import TopologyRing
from TopologySparse import TopologySparse
from TopologyFull import TopologyFull


SELFISH_GROUP_IDS = []  # wait; depends on how many honest pools there are


class Simulator(object):
    """Class to handle a selfish mining simulation.
    
    Attributes:
        time (float): Time elapsed (seconds) since the start of the simulation.
        topology: A Topology object handling the network graph and pools.
        nodes: A list of nodes in the simulation. Nodes may be honest or
            selfish, and may be mining or non-mining.
        events: A priority queue of SendBlockEvents and MineBlockEvents.
    """
    def __init__(self, honestPoolSizes, honestPoolPowers,
                 nSelfishClients, nSelfishMiners, alpha):
        """Creates a selfish mining simulation with one or more honest pools of
        size `honestPoolSizes` (list of ints) and power `honestPoolPowers` (list
        of floats), plus one selfish mining pool with `nSelfishClients` (int)
        non-mining nodes and `nSelfishMiners` (int) mining nodes with combined
        hashpower `alpha` (float). All miners in a given pool split hashpower
        equally.
        
        Logs events as they 'occur' (get popped off the priority queue).
        
        Preconditions:
        len(honestPoolSizes) == len(honestPoolPowers)
        sum(honestPoolPowers) + alpha == 1
        """
        global SELFISH_GROUP_IDS
        SELFISH_GROUP_IDS = []
        if len(honestPoolSizes) != len(honestPoolPowers):
            assert False, `len(honestPoolSizes)` + " != " + `len(honestPoolPowers)`
        # stupid float rounding errors
        if abs(sum(honestPoolPowers) + alpha - 1) > 1e-13:
            assert False, '+'.join(map(str, honestPoolPowers+[alpha])) + " == " + `sum(honestPoolPowers) + alpha` + " != 1"
        
        self._set_up_logging()
        self._set_up_nodes(honestPoolSizes, honestPoolPowers, nSelfishClients,
                           nSelfishMiners, alpha)
        self._set_up_topology(honestPoolSizes)
        self._set_up_events()
    
    def _set_up_logging(self):
        """Initializes time to 0 and creates the beginning of the logfile."""
        self.time = 0.0
        startTimeString = time.strftime("%Y-%m-%d_%H.%M.%S")
        logging.basicConfig(filename='logfile%s.log' % startTimeString, level=logging.DEBUG)
        logging.info('Beginning simulation at %s.' % startTimeString)
    
    def _set_up_nodes(self, honestPoolSizes, honestPoolPowers, nSelfishClients,
                      nSelfishMiners, alpha):
        global SELFISH_GROUP_IDS
        
        self.nodes = []
        
        for groupId in range(len(honestPoolSizes)):
            thisPoolPower = honestPoolPowers[groupId]
            thisPoolSize = honestPoolSizes[groupId]
            for eachMiner in range(thisPoolSize):
                # the Miner constructor takes care of blockchain initialization
                self.nodes.append(Miner(thisPoolPower/thisPoolSize, groupId))
        
        selfishGroupId = groupId + 1
        SELFISH_GROUP_IDS.append(selfishGroupId)
        for _ in range(nSelfishClients):
            self.nodes.append(SelfishMiner(0, selfishGroupId))
        for _ in range(nSelfishMiners):
            self.nodes.append(
                SelfishMiner(alpha/nSelfishMiners, selfishGroupId)
            )
    
    def _set_up_topology(self, honestPoolSizes):
        """Helper to set up the topology for this simulation."""
        self.topology = TopologySparse(
            self.nodes
        )
        
        for groupId in range(len(honestPoolSizes)):
            poolMembers = [n for n in self.nodes if n.groupId==groupId]
            assert len(poolMembers) == honestPoolSizes[groupId]
            self.topology.addGroup(groupId, poolMembers)
        
        for selfishGroupId in SELFISH_GROUP_IDS:
            self.topology.addGroup(
                selfishGroupId,
                [n for n in self.nodes if n.groupId==selfishGroupId]
            )
    
    def _set_up_events(self):
        """Creates the first mining events off the genesis block."""
        self.events = []
        for miner in [n for n in self.nodes if n.power > 0]:
            heapq.heappush(self.events, miner.getFutureMineEvent(self.time))
    
    def run(self, maxBlocks):
        """Runs the simulation until int maxBlocks have been mined.
        
        For each event popped from the queue, handle it and log it. This gets
        any resulting subsequent events; add them to the queue and continue!
        """
        logging.info('Running for %i blocks.' % maxBlocks)
        
        nBlocks = 0
        while nBlocks < maxBlocks:
            try:
                event = heapq.heappop(self.events)  # get an event
                self.time = event.time  # update current time
                if isinstance(event, MineBlockEvent):
                    nBlocks += 1
                subsequentEvents = event.handle()  # handle the event
                [heapq.heappush(self.events, e) for e in subsequentEvents]
            except IndexError:
                continue
        return
    
    def printStatistics(self):
        """Print information about how many blocks each miner and group has
        generated."""
        honestBlocks = 0
        selfishBlocks = 0
        
        # try to consult a selfish node (if any) to see where the public and
        # private chains diverge; cut off blocks past this point
        # also exclude the genesis block
        nodeToConsult = None
        for node in self.nodes:
            if node.isSelfish:
                nodeToConsult = node
        if not nodeToConsult:
            # fallback to honest node and plain stats
            nodeToConsult = self.nodes[0]
        
        mainchain = nodeToConsult.blockchain.getMainChain()
        if nodeToConsult.isSelfish:
            successfulMinedBlocks = mainchain[1:nodeToConsult.publicBranchStart]
        else:
            successfulMinedBlocks = mainchain[1:]
        
        for block in successfulMinedBlocks:
            if block.groupId in SELFISH_GROUP_IDS:
                selfishBlocks += 1
            else:
                honestBlocks += 1
        
        output = "Honest Blocks: " + str(honestBlocks)
        output += "\nSelfish Blocks: " + str(selfishBlocks)
        try:
            proportion = selfishBlocks/(honestBlocks+selfishBlocks)
        except ZeroDivisionError:
            proportion = "N/A"
        output += "\nProportion: " + str(proportion)
        
        if nodeToConsult.isSelfish:
            output += '\nPublic Chain Len: ' + str(nodeToConsult.getPublicChainLen())
            output += '\nPrivate Chain Len: ' + str(nodeToConsult.privateBranchLen)
        
        logging.info(output)
        print output
