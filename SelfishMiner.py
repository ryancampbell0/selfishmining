from Miner import Miner
from MineBlockEvent import MineBlockEvent
from Block import Block
import logging


class SelfishMiner(Miner):
    """Represents a selfish miner, who is most likely but not necessarily part
    of a pool. May have zero hashpower, if they are a non-mining node in the
    pool that serves to participate in the needed sibyl attack.
    
    Additional (Non-Inherited) Attributes:
        privateUnpublished (list of Blocks) the unpublished part of the private
            chain held by this miner's pool
        privateBranchLen (int): the length of the pool's private chain. Distinct
            from len(privateUnpublished) as the selfish mining pool may publish
            parts of their private chain at times.
        publicBranchStart (int): equal to the number of blocks which the public
            main chain and the private main chain share (min. 1, the genesis
            block). If the genesis block has height 0, then the first private
            block will have height == publicBranchStart. Used to determine the
            length of the public chain (must store as index rather than list of
            blocks since the public chain may fork).
    """
    def __init__(self, power, groupId):
        Miner.__init__(self, power, groupId, True)
        self.privateUnpublished = []
        self.privateBranchLen = 0
        self.publicBranchStart = 1
    
    def getPublicChainLen(self):
        """Using the publicBranchStart attribute, gets the length of the public
        chain as depicted in the selfish mining algorithm. Stored as an index
        rather than as the blocks directly in case the public chain forks."""
        return len(self.blockchain.getMainChain()) - self.publicBranchStart
    
    def hasThisBlock(self, blockId):
        inPublic = self.blockchain.contains(blockId)
        inPrivate = blockId in [b.id for b in self.privateUnpublished]
        return inPublic or inPrivate
    
    def _publishPrivateChain(self, currentTime, logEntry):
        events = self.topology.sendBlocks(currentTime, self.id, self.privateUnpublished)
        self.blockchain.addBlocks(self.privateUnpublished)
        self.publicBranchStart += self.privateBranchLen
        self.privateUnpublished = []
        self.privateBranchLen = 0
        logging.info(logEntry)
        return events
    
    def _isFellowPoolMember(self, otherMinerId):
        return any([m.id == otherMinerId for m in self.topology.groups[self.groupId]])
    
    def receiveBlocks(self, srcId, currentTime, blocks, logEntry):
        """Same specification as Miner.receiveBlocks:
        
        Receives a notification of the creation of a new block(s) (either by
        itself or by a message passed by another node). Adds the received
        block(s) to the blockchain in the correct spot. Ensures that the
        block(s) are not already present; if they are, do not add it again and
        do not send it to neighbors.
        
        If the blocks are added to the blockchain, log the provided log entry.
        
        Return the subsequent SendBlockEvents if any (i.e. the messages that
        will propogate this block to its neighbors). Since this is a selfish
        miner, it is possible that no network propogation will occur."""
        events = []
        publicChainLen = self.getPublicChainLen()
        delta = self.privateBranchLen - publicChainLen
        
        # different behavior if receiving a block with the same groupId from a
        # fellow selfish pool member (both conditions must be met) - treat as
        # private block and add to private chain.
        # minor kludge: a selfish miner never has to receive a pool's block from
        # a fellow member with the intent to publish it - the miner would have
        # already published it itself, thanks to the intranet keeping all pool
        # members in sync private-branch-wise with zero latency
        if len(blocks) >= 2:
            assert all([b.groupId == blocks[0].groupId for b in blocks])
        if self._isFellowPoolMember(srcId) and blocks[0].groupId == self.groupId:
            if all([self.hasThisBlock(b.id) for b in blocks]):
                return []
            # my pool found a block
            assert len(blocks) == 1
            block = blocks[0]
            logging.info(logEntry + " secretly")
            # append new block to private chain
            self.privateUnpublished.append(block)
            self.privateBranchLen += 1
            if delta == 0 and self.privateBranchLen == 2:
                # was race; we just won
                # event (b) in paper
                events.extend(self._publishPrivateChain(currentTime, logEntry))
            #else:
                # event (a) in paper
        else:
            # others found a block(s)
            # includes case where honest block is being propagated among selfish
            # miners
            if all([self.blockchain.contains(b.id) for b in blocks]):
                return []
            self.blockchain.addBlocks(blocks)
            # propagate blocks both through intranet and network
            events.extend(self.topology.sendBlocks(currentTime, self.id, blocks, self.groupId))
            # kludge: selfish nodes purposely delay 0.1 sec when sending honest
            # nodes through the network (as opposed to intranet), to guarantee
            # race win during sibyl attack
            events.extend(self.topology.sendBlocks(currentTime+self.topology.max_network_latency(), self.id, blocks))
            logging.info(logEntry)
            if delta == 0:
                # they win
                self.privateUnpublished = []
                self.privateBranchLen = 0
                # handles both the case where there is no private chain and they
                # win (event (e) in paper), and the case where there is a race
                # and they win (events (c) and (d) in paper)
                self.publicBranchStart = len(self.blockchain.getMainChain())
            elif delta == 1:
                # begin race
                # publish last block of the private chain
                # event (f) in paper
                secretBlock = self.privateUnpublished.pop()
                self.blockchain.addBlocks([secretBlock])
                events.extend(self.topology.sendBlocks(currentTime, self.id, [secretBlock]))
                logging.info(logEntry)
            elif delta == 2:
                # event (g) in paper
                events.extend(self._publishPrivateChain(currentTime, logEntry))
            elif delta > 2:
                # publish first unpublished block in private chain
                # event (h) in paper
                secretBlock = self.privateUnpublished.pop(0)
                self.blockchain.addBlocks([secretBlock])
                events.extend(self.topology.sendBlocks(currentTime, self.id, [secretBlock]))
            else:
                assert False, "delta is negative! " + str(delta)
        return events
    
    def mineBlock(self, time):
        """Mines a new block, then secretly notifies all pool members (including
        itself) of that block's arrival."""
        if self.privateUnpublished:
            prev_head_block = self.privateUnpublished[-1]
        else:
            prev_head_block = self.blockchain.head()
        new_block = Block(prev_head_block.id, time, self.id, self.groupId)
        logEntry = self.logMinedBlock(time, new_block)
        logging.info(logEntry)
        # secretly send to pool members only
        events = self.topology.sendBlocks(time, self.id, [new_block], self.groupId)
        return events
