from Topology import Topology
import logging


class TopologyFull(Topology):
    """A fully-connected topology. All nodes are neighbors of all other nodes.
    """
    def __init__(self, nodes):
        """Create a new fully-connected Topology using the given input nodes
        (Miner objects)."""
        logging.info('Creating a fully-connected topology.')
        self.graph = {}
        self.groups = {}
        
        for node in nodes:
            node.topology = self
            
            all_other_nodes = [other for other in nodes if other.id != node.id and other.groupId != node.groupId]
            self._addClient(node.id, all_other_nodes)
